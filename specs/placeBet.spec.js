describe('Place Bet', () => {
  it('user is able to place single bet', async () => {
    await browser.url('https://williamhill-de-mtt.sit.unity.williamhill-dev.com/');
    await $('div[am-modal-buttonswrapper] button').click();
    await $('button[am-siteheader-button]').click();
    await $('input[data-testid="username"]').setValue('Ernestina.Hauck.5@williamhill.com');
    await $('input[data-testid="password"]').setValue('Abcd1234');
    await $('button[type="submit"]').click();
    await browser.pause(2500);  //this is ugly, but it's caused by app bug, DON'T DO EXAMPLE
    const selections = await $$('button[am-button="price  "]');
    await selections[18].click();
    await $('div[am-betslip-input-wrapper]').click();
    await $$('button[am-betslipkeyboard-key]')[0].click();
    await $$('button[am-betslipkeyboard-key]')[1].click();
    await $('button[am-betslip-button]').click();
    await $('div[am-betslip-donebutton] > button').click();
    await browser.pause(5000);  //this is just to demonstrate the test is passing
  })
})